﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Comon2
{
    public class MailHelper
    {
        public void SendMail(string toEmailAddress, string subject, string content, string pathFile)
        {
            var fromEmailAddress = ConfigurationManager.AppSettings["FromEmailAddress"].ToString();
            var fromEmailDisplayName = ConfigurationManager.AppSettings["FromEmailDisplayName"].ToString();
            var fromEmailPassword = ConfigurationManager.AppSettings["FromEmailPassword"].ToString();
            var sMTPHost = ConfigurationManager.AppSettings["SMTPHost"].ToString();
            var sMTPPort = ConfigurationManager.AppSettings["SMTPPort"].ToString();

            bool enabledSSL = bool.Parse(ConfigurationManager.AppSettings["EnabledSSL"].ToString());

            string body = content;
            MailMessage message = new MailMessage(new MailAddress(fromEmailAddress, fromEmailDisplayName), new MailAddress(toEmailAddress));
            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Body = body;

            Attachment attachment = new Attachment(pathFile);
            message.Attachments.Add(attachment);

            var client = new SmtpClient();
            client.Credentials = new NetworkCredential(fromEmailAddress, fromEmailPassword);
            client.Host = sMTPHost;
            client.EnableSsl = enabledSSL;
            client.Port = !string.IsNullOrEmpty(sMTPPort) ? Convert.ToInt32(sMTPPort) : 0;
            client.Send(message);
        }
    }
}
