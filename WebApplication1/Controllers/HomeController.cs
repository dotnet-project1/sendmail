﻿using Comon2;
using System;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Mail(HttpPostedFileBase file)
        {

            string _path = "";

            try
            {
                if (file.ContentLength > 0)
                {
                    string _FileName = Path.GetFileName(file.FileName);
                     _path = Path.Combine(Server.MapPath("~/assets/img"), _FileName);

                    file.SaveAs(_path);
                }
            }
            catch
            {
                ViewBag.Message = "Send mail failed!!!";
            }

            // get info from view
            var from = ConfigurationManager.AppSettings["FromEmailAddress"].ToString();
            var to = Request["to"];
            var subject = Request["subject"];
            var notes = Request["notes"];
            var content = System.IO.File.ReadAllText(Server.MapPath("~/assets/templateMail/mail.html"));

            content = content.Replace("{{From}}", from);
            content = content.Replace("{{To}}", to);
            content = content.Replace("{{Subject}}", subject);
            content = content.Replace("{{Notes}}", notes);

            System.Diagnostics.Debug.WriteLine("ok", to);

            // send mail
            new MailHelper().SendMail(to, subject, content, _path);
            ViewBag.Message = "Send mail to \"" + to + "\" success";
            return View("Result");
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}